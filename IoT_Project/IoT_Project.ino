/*-----------Deskripsi Pin Pada LCD-----------------
Pin 1 : GND
Pin 2 : Vcc
Pin 3 : VEE
Pin 4 : Register Selecy
Pin 5 : Read / Write
Pin 6 : Enable
Pin 7 : Data 0
Pin 8 : Data 1
Pin 9 : Data 2
Pin 10 : Data 3
Pin 11 : Data 4
Pin 12 : Data 5
Pin 13 : Data 6
Pin 14 : Data 7
Pin 15 : LCD Light +
Pin 16 : LCD Light -
// lcd(RS, Enable, D4, D5, D6, D7);
---------------------------------------------------*/

#include<LiquidCrystal.h>
#include<dht11.h>
#include <Ethernet.h>
#include <SPI.h>

#define DHT_SENSOR_PIN 24
#define STATUS_CONNECTED 1
#define STATUS_DISCONNECTED 0

//char namaServer[] = "169.254.110.195";
char namaServer[] = "iot-project.laurensius-dede-suhardiman.com";
//char namaServer[] = "iot-project.synergize.co";
//char namaServer[] = "192.168.0.102";


//byte IP_eth[] = {169,254,110,196};
byte IP_eth[] = {192,168,0,110};
byte MAC_eth[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };


int respon_dht11;
int counter = 0;

boolean startRead = false; 

char inString[32];
char charFromWeb[9];

LiquidCrystal lcd(28 ,26 ,5 ,4 ,3 ,2); 
dht11 sensor_dht;
EthernetClient myEthernet;

int iterasi = 0;

void setup(){
  Serial.begin(9600);
  Serial.println("--------------------------------------------------"); 
  Serial.println("Setting Perangkat");
  Serial.println("Mohon menunggu . . . ");
  Serial.println("Setting Ethernet MAC Address dan IP Address");
  Serial.println("Mohon menunggu . . . ");
  if (Ethernet.begin(MAC_eth) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    Ethernet.begin(MAC_eth,IP_eth);
  }
 // Ethernet.begin(MAC_eth,IP_eth);
  Serial.println("Setting Sensor Suhu dan Kelembaban");
  Serial.println("Mohon menunggu . . . ");
  Serial.print("Versi Library DHT : ");
  Serial.println(DHT11LIB_VERSION);
  lcd.begin(16,2);
  lcd.setCursor(0,0);
  lcd.print("DHT11 Lib : ");
  lcd.setCursor(0,1);
  lcd.print(DHT11LIB_VERSION);
  delay(1000);
  lcd.clear();
  Serial.println("Setting Perangkat selesai!");
  Serial.println("--------------------------------------------------");
}

void loop() {
  iterasi++;
  Serial.print("Iterasi ke : ");
  Serial.println(iterasi);
  inisialisasi_dht11();
  String a = ambil_data_dht11();
  int resultBukaKoneksi = bukaKoneksi();
  if(resultBukaKoneksi==1){
      kirimData(a);
      Serial.println();
  }
  delay(1000); 
  Serial.println("--------------------------------------------------");
}

int bukaKoneksi(){
  Serial.print("Mencoba sambungan ke server http://"); 
  Serial.println(namaServer);  
  Serial.println("Mohon menunggu . . . ");
  if(myEthernet.connect(namaServer,80)){
    Serial.println("Sambungan ke server berhasil!");
    return STATUS_CONNECTED; 
  }else{
    Serial.print("Sambungan ke server gagal!");
    Serial.println();
    return STATUS_DISCONNECTED;
  }
}

void kirimData(String a){
    Serial.println("Menjalankan perintah kirim data");
    String data = " Arduino";
    int ln = data.length();
    String uri_segment;
    uri_segment = "/index.php/device/post_data/" + a;
//    uri_segment = "/iot_server/index.php/device/post_data/" + a; 
    myEthernet.print("GET ");
    myEthernet.print(uri_segment); 
    Serial.print("Data yang dikirim di ke server : ");
    Serial.println(a);
    myEthernet.println(" HTTP/1.0");
    myEthernet.print( "Host: " );
    myEthernet.println(" iot-project.laurensius-dede-suhardiman.com \r\n");
//    myEthernet.println(" 192.168.0.102 \r\n");
    Serial.println("Host OK");
    myEthernet.println( "Content-Type: application/x-www-form-urlencoded \r\n" );
    Serial.println("Content type OK");
    myEthernet.print( "Content-Length: " );
    myEthernet.print(ln);
    myEthernet.print(" \r\n");
    myEthernet.println( "Connection: close" );
    myEthernet.println();
    String res;
    res = bacaWebText();
    if(res.equals("")==false){
      Serial.println("Data suhu dan kelembaban tersimpan.");
      Serial.print("Jumlah rows database ada : ");
      Serial.println(res);
    }
//------------warning selalu tutup koneksi ya........
//    myEthernet.stop();
//    myEthernet.flush();
//--------------------------------------------------
}

String bacaWebText(){
  unsigned int time;
  Serial.println("Baca respon dari server . . . "); 
  Serial.println("Mohon menunggu . . . ");
  time = millis();
  Serial.print("Timer Millis () : ");
  Serial.println(time);
  int stringPos = 0;
  memset( &inString, 0, 32 );
  int unvailable_ctr = 0;
  while(true){
    if (myEthernet.available()) {
      char c = myEthernet.read();
      Serial.print(c);
      if (c == '#' ) { 
        Serial.print("Menemukan start key # dengan isi : ");
        startRead = true;  
      }else if(startRead){
        if(c != '^'){ 
          inString[stringPos] = c;
          stringPos ++;
        }else{
          startRead = false;
          Serial.println();
          Serial.println("Baca respon dari server selesai!");
          myEthernet.stop();
          myEthernet.flush();
          Serial.println("Sambungan diputuskan . . . ");
          return inString;
        }
      }
    }else{
       //Serial.println("ethernet unavailable");
       delay(50);
       unvailable_ctr++;
       if(unvailable_ctr == 25){
         myEthernet.stop();
         myEthernet.flush();
         Serial.println("Koneksi mengalami time out");
         Serial.println("Sambungan diputuskan . . . ");
         Serial.println("Reset...");
         return inString;
       }
    }
  }
}

void inisialisasi_dht11(){
  respon_dht11 = sensor_dht.read(DHT_SENSOR_PIN);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("DHT11 Lib : ");
  switch (respon_dht11){
    case DHTLIB_OK:  
      Serial.println("Sensor DHT status : OK"); 
      lcd.setCursor(0,1);
      lcd.print("Status OK");
      break;
    case DHTLIB_ERROR_CHECKSUM: 
      Serial.println("Sensor DHT status : Checksum error"); 
      lcd.setCursor(0,1);
      lcd.print("Checksum error");
      break;
    case DHTLIB_ERROR_TIMEOUT: 
      Serial.println("Sensor DHT status : Time out error"); 
      lcd.setCursor(0,1);
      lcd.print("Time out error");
      break;
    default: 
      Serial.println("Sensor DHT status : Unknown error"); 
      lcd.setCursor(0,1);
      lcd.print("Unknown error");
      break;
  }
  delay(1000);
  lcd.clear();
}

String ambil_data_dht11(){
  Serial.print("Temperatur : ");
  Serial.print(sensor_dht.temperature);
  Serial.print(",\t");
  Serial.print("Kelembaban : ");
  Serial.println(sensor_dht.humidity);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Temperatur : ");
  lcd.setCursor(14,0);
  lcd.print(sensor_dht.temperature);
  lcd.setCursor(0,1);
  lcd.print("Kelembaban : ");
  lcd.setCursor(14,1);
  lcd.print(sensor_dht.humidity);
  delay(2000);
  String temperature = String(sensor_dht.temperature);
  String humidity = String(sensor_dht.humidity);
  String data = temperature + "/" + humidity;
  return data;
}

